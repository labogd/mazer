package mazer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mazer.grid.ArrayGrid;
import mazer.grid.Grid;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        final int size = 80;
        final Grid<Integer> maze = ArrayGrid.of(size, size);
        maze.apply(location -> location.row() >= 35 && location.row() < 45 && location.column() >= 35 && location.column() < 45 ? Maze.AISLE : Maze.WALL);
        final Scene scene = new Scene(new Mazer(maze, 640 / size));
        stage.setScene(scene);
        stage.setTitle("Mazer");
        stage.show();
    }
}
