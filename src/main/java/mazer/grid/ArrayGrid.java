package mazer.grid;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ArrayGrid<E> implements Grid<E> {

    private final int rows;
    private final int columns;
    private E[] elements;
    private E[] backBuffer;
    private final GridIndexer indexer;

    private ArrayGrid(int rows, int columns, E[] elements, E[] backBuffer, GridIndexer indexer) {
        this.rows = rows;
        this.columns = columns;
        this.elements = elements;
        this.backBuffer = backBuffer;
        this.indexer = indexer;
    }

    public static <E> Grid<E> of(int rows, int columns) {
        return new ArrayGrid<>(rows, columns, (E[]) new Object[rows * columns], (E[]) new Object[rows * columns], new GridIndexer(rows, columns, 0, 0));
    }

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int columns() {
        return columns;
    }

    @Override
    public Grid<E> fill(E element) {
        Arrays.parallelSetAll(backBuffer, i -> element);
        flip();
        return this;
    }

    @Override
    public Grid<E> apply(Function<Grid.Location<E>, E> mapper) {
        IntStream.range(0, rows).parallel()
                .forEach(row -> IntStream.range(0, columns)
                        .forEach(column -> backBuffer[indexer.toArrayIndex(row, column)] = mapper.apply(at(row, column))));
        flip();
        return this;
    }

    @Override
    public Grid.Location<E> at(int row, int column) {
        return new Location(row, column);
    }

    @Override
    public Stream<Grid.Location<E>> locations() {
        return IntStream.range(0, rows).boxed()
                .flatMap(row -> IntStream.range(0, columns)
                        .mapToObj(column -> new Location(row, column)));
    }

    private void flip() {
        final E[] flipped = elements;
        elements = backBuffer;
        backBuffer = flipped;
    }

    private class Location implements Grid.Location<E> {

        private final int row;
        private final int column;

        public Location(int row, int column) {
            this.row = row;
            this.column = column;
        }

        @Override
        public int row() {
            return row;
        }

        @Override
        public int column() {
            return column;
        }

        @Override
        public E value() {
            return elements[indexer.toArrayIndex(row, column)];
        }

        @Override
        public Grid<E> set(E element) {
            elements[indexer.toArrayIndex(row, column)] = element;
            return ArrayGrid.this;
        }

        @Override
        public Optional<Grid.Location<E>> moveBy(int rows, int columns) {
            int newRow = row + rows;
            int newColumn = column + columns;
            if (newRow < 0 || newRow >= ArrayGrid.this.rows || newColumn < 0 || newColumn >= ArrayGrid.this.columns) {
                return Optional.empty();
            }
            return Optional.of(new Location(newRow, newColumn));
        }

        @Override
        public Grid<E> span(int rows, int columns) {
            final GridIndexer newIndexer = new GridIndexer(indexer.rows, indexer.columns, indexer.rowOffset + row, indexer.columnOffset + column);
            return new ArrayGrid<>(rows, columns, elements, backBuffer, newIndexer);
        }

        @Override
        public boolean equals(Object object) {
            if (!(object instanceof Grid.Location)) {
                return false;
            }
            final Grid.Location<E> other = (Grid.Location<E>) object;
            return row == other.row() && column == other.column();
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + this.row;
            hash = 47 * hash + this.column;
            return hash;
        }
    }

    private static class GridIndexer {

        private final int rows;
        private final int columns;
        private final int rowOffset;
        private final int columnOffset;

        public GridIndexer(int rows, int columns, int rowOffset, int columnOffset) {
            this.rows = rows;
            this.columns = columns;
            this.rowOffset = rowOffset;
            this.columnOffset = columnOffset;
        }

        public int toArrayIndex(int row, int column) {
            return ((rowOffset + row) * columns) + (columnOffset + column);
        }
    }
}
