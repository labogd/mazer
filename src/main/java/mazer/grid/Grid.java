package mazer.grid;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public interface Grid<E> {

    int rows();

    int columns();

    Grid<E> fill(E element);

    Grid<E> apply(Function<Location<E>, E> mapper);

    Location<E> at(int row, int column);

    Stream<Location<E>> locations();

    interface Location<E> {

        int row();

        int column();

        E value();

        Grid<E> set(E element);

        Optional<Location<E>> moveBy(int rows, int columns);

        Grid<E> span(int rows, int columns);
    }
}
