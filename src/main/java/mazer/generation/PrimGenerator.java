package mazer.generation;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import mazer.Maze;
import mazer.grid.Grid;

public class PrimGenerator<V, E> implements MazeGenerator {

    private final List<Wall> walls;
    private final Random random;

    public PrimGenerator(int initialRow, int initialColumn, Random random) {
        this.random = random;
        walls = new LinkedList<>();
        addCellWalls(initialRow, initialColumn);
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
        final Optional<Wall> nextWall = chooseWall(maze);
        return nextWall.map(wall -> {
            addCellWalls(wall.endRow, wall.endColumn);
            return maze.apply(location -> {
                if (location.row() == wall.startRow && location.column() == wall.startColumn) {
                    return Maze.AISLE;
                }
                if (location.row() == wall.endRow && location.column() == wall.endColumn) {
                    return Maze.AISLE;
                }
                if (location.row() == (wall.startRow + wall.endRow) / 2 && location.column() == (wall.startColumn + wall.endColumn) / 2) {
                    return Maze.AISLE;
                }
                return location.value();
            });
        }).orElse(maze);
    }

    @Override
    public boolean isEnded() {
        return walls.isEmpty();
    }

    private void addCellWalls(int row, int column) {
        walls.add(new Wall(row, column, row - 2, column));
        walls.add(new Wall(row, column, row + 2, column));
        walls.add(new Wall(row, column, row, column - 2));
        walls.add(new Wall(row, column, row, column + 2));
    }

    private Optional<Wall> chooseWall(Grid<Integer> maze) {
        Optional<Wall> wall = Optional.empty();
        while (!wall.isPresent() && !walls.isEmpty()) {
            final Wall candidate = walls.remove(random.nextInt(walls.size()));
            if (wallInMazeBounds(candidate, maze) && maze.at(candidate.endRow, candidate.endColumn).value() == Maze.WALL) {
                wall = Optional.of(candidate);
            }
        }
        return wall;
    }

    private boolean wallInMazeBounds(Wall wall, Grid<Integer> maze) {
        return wall.endRow >= 0 && wall.endColumn >= 0 && wall.endRow < maze.rows() && wall.endColumn < maze.columns();
    }

    private static class Wall {

        public final int startRow;
        public final int startColumn;
        public final int endRow;
        public final int endColumn;

        public Wall(int startRow, int startColumn, int endRow, int endColumn) {
            this.startRow = startRow;
            this.startColumn = startColumn;
            this.endRow = endRow;
            this.endColumn = endColumn;
        }
    }
}
