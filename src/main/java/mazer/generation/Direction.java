package mazer.generation;

public enum Direction {

    UP(-1, 0),
    DOWN(1, 0),
    LEFT(0, -1),
    RIGHT(0, 1);

    private final int rowOffset;
    private final int columnOffset;

    private Direction(int rowOffset, int columnOffset) {
        this.rowOffset = rowOffset;
        this.columnOffset = columnOffset;
    }

    public int rowOffset() {
        return rowOffset;
    }

    public int columnOffset() {
        return columnOffset;
    }
}
