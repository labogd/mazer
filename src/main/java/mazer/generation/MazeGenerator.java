package mazer.generation;

import mazer.grid.Grid;

public interface MazeGenerator {

    Grid<Integer> advance(Grid<Integer> maze);

    default boolean isEnded() {
        return false;
    }
}
