package mazer.generation;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import javafx.util.Pair;
import mazer.Maze;
import mazer.grid.Grid;

public class DepthFirstGenerator implements MazeGenerator {

    private final Deque<Pair<Integer, Integer>> path = new LinkedList<>();
    private Direction direction;
    private final Random random;

    public DepthFirstGenerator(int initialRow, int initialColumn, Direction direction, Random random) {
        path.push(new Pair<>(initialRow, initialColumn));
        this.random = random;
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
        if (path.isEmpty()) {
            return maze;
        }
        final Pair<Integer, Integer> cell = path.peek();
        maze.at(cell.getKey(), cell.getValue()).set(Maze.AISLE);
        final List<Direction> neighbours = nonVisitedNeighbours(maze, cell.getKey(), cell.getValue());
        if (neighbours.isEmpty()) {
            path.remove();
            return advance(maze);
        }
        chooseDirection(neighbours);
        maze.at(cell.getKey() + direction.rowOffset(), cell.getValue() + direction.columnOffset()).set(Maze.AISLE);
        path.push(new Pair<>(cell.getKey() + direction.rowOffset() * 2, cell.getValue() + direction.columnOffset() * 2));
        return maze;
    }

    @Override
    public boolean isEnded() {
        return path.isEmpty();
    }

    private List<Direction> nonVisitedNeighbours(Grid<Integer> maze, int row, int column) {
        final List<Direction> neighbours = new ArrayList<>();
        if (row >= 2 && maze.at(row - 2, column).value() == Maze.WALL) {
            neighbours.add(Direction.UP);
        }
        if (row < maze.rows() - 2 && maze.at(row + 2, column).value() == Maze.WALL) {
            neighbours.add(Direction.DOWN);
        }
        if (column >= 2 && maze.at(row, column - 2).value() == Maze.WALL) {
            neighbours.add(Direction.LEFT);
        }
        if (column < maze.columns() - 2 && maze.at(row, column + 2).value() == Maze.WALL) {
            neighbours.add(Direction.RIGHT);
        }
        return neighbours;
    }

    private void chooseDirection(List<Direction> availableDirections) {
        if (!availableDirections.contains(direction) || random.nextDouble() >= 0.75) {
            direction = availableDirections.get(random.nextInt(availableDirections.size()));
        }
    }
}
