package mazer.path;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import mazer.Maze;
import mazer.grid.Grid;

public class BFS implements MazeVisitor {

    private final Set<VisitedCell> visiting;
    private int lastValue = 0xFF;

    public BFS(int startRow, int startColumn) {
        visiting = new HashSet<>();
        visiting.add(new VisitedCell(startRow, startColumn, lastValue));
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
        final List<VisitedCell> cells = new ArrayList<>(visiting);
        visiting.clear();
        for (VisitedCell cell : cells) {
            nonVisitedNeighbours(maze, cell).forEach(visiting::add);
            maze = maze.at(cell.row, cell.column).set(cell.value);
        }
        return maze;
    }

    @Override
    public boolean isEnded() {
        return visiting.isEmpty();
    }

    private Stream<VisitedCell> nonVisitedNeighbours(Grid<Integer> maze, VisitedCell cell) {
        if (cell.value == lastValue) {
            lastValue = advanceValue(cell.value);
        }
        return Stream.of(
                visitAt(maze, cell.row - 1, cell.column, lastValue),
                visitAt(maze, cell.row + 1, cell.column, lastValue),
                visitAt(maze, cell.row, cell.column - 1, lastValue),
                visitAt(maze, cell.row, cell.column + 1, lastValue)
        ).filter(Optional::isPresent).map(Optional::get);
    }

    private Optional<VisitedCell> visitAt(Grid<Integer> maze, int row, int column, int newValue) {
        if (row < 0 || row >= maze.rows() || column < 0 || column >= maze.columns()) {
            return Optional.empty();
        }
        return maze.at(row, column).value() == Maze.AISLE ? Optional.of(new VisitedCell(row, column, newValue)) : Optional.empty();
    }

    private static int advanceValue(int value) {
        if ((value & 0xFF) == 0xFF) {
            if (((value >> 8) & 0xFF) == 0xFF) {
                return 0xFFFE;
            }
            if (((value >> 16) & 0xFF) == 0) {
                return value + 0x100;
            }
            return value - 0x10000;
        }
        if (((value >> 8) & 0xFF) == 0xFF) {
            if (((value >> 16) & 0xFF) == 0xFF) {
                return 0xFFFE00;
            }
            if ((value & 0xFF) == 0) {
                return value + 0x10000;
            }
            return value - 0x1;
        }
        if ((value & 0xFF) == 0xFF) {
            return 0xFE00FF;
        }
        if (((value >> 8) & 0xFF) == 0) {
            return value + 0x1;
        }
        return value - 0x100;
    }

    private static class VisitedCell {

        public final int row;
        public final int column;
        public final int value;

        public VisitedCell(int row, int column, int value) {
            this.row = row;
            this.column = column;
            this.value = value;
        }

        @Override
        public boolean equals(Object object) {
            final VisitedCell other = (VisitedCell) object;
            return row == other.row && column == other.column;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + this.row;
            hash = 53 * hash + this.column;
            return hash;
        }
    }
}
