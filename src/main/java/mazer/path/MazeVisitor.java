package mazer.path;

import mazer.grid.Grid;

public interface MazeVisitor {

    Grid<Integer> advance(Grid<Integer> maze);

    default boolean isEnded() {
        return false;
    }
}
