# Mazer

Generate mazes vith various algorithms.


## Compile

Requires Java 8 and Gradle.
Compile and run with:

```
gradle build
java -jar build/libs/mazer.jar
```
